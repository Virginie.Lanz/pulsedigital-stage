const questions = [
    {
        questionText: 'Chaud ou froid ?',
        answerOptions: [
            { answerText: 'Chaud', isSelected: 'Plasmacom'},
            { answerText: 'Froid', isSelected: 'Pulse' }
        ],
    },
    {
        questionText: 'Ambiance ?',
        answerOptions: [
            { answerText: 'Calme', isSelected: 'NKCO' },
            { answerText: 'Animé', isSelected: 'Rekovelle' }
        ],
    },
    {
        questionText: 'Parler la langue locale ?',
        answerOptions: [
            { answerText: 'Langue parlée', isSelected: 'Plasmacom' },
            { answerText: 'Langue inconnue', isSelected: 'Pulse' }
        ],
    },
    {
        questionText: 'Durée du séjour ?',
        answerOptions: [
            { answerText: 'Court séjour', isSelected: 'NKCO' },
            { answerText: 'Long séjour', isSelected: 'Rekovelle' }
        ],
    },
]

export default questions