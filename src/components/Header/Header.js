import React from 'react'
import styled from 'styled-components'

export default function Header() {
	return (
		<Global className="header container">
			<Nav className="text text--para">Virginie <span className="text--primary">+ PULSE.digital</span></Nav>
		</Global>
	);
}

const Global = styled.div`
background: white;
z-index: 10;
position: relative;
height: 80px;
display: flex;
align-items: center;
`;
const Nav = styled.nav``;

