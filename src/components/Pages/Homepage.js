import React from 'react'
import Header from '../Header/Header'
import styled from 'styled-components'
import { NavLink } from 'react-router-dom'

export default function Homepage() {
	return (
		<Global className="homepage">
            <Header/>
            <Content className="content  container">
                <div className="content__text">
                    <h1 className="text text--title">
                        <span className="text--primary">PULSE.digital </span> 
                        est une <span className="text--primary">aventure</span>. 
                        Quelle sera votre <span className="text--primary">destination</span>&nbsp;?
                    </h1>
                </div>
                <div className="content__btn">
                    <Button className="btn btn--primary text text--para">
                        <NavLink to="/quiz">Commencer <span class="material-icons">arrow_right_alt</span></NavLink>
                    </Button>
                </div>
            </Content>
		</Global>
	);
}

const Global = styled.div``;

const Content = styled.div`
//Header is 80px. Bottom 80px/2
height: calc(100vh - 120px);
display: flex;
flex-direction: column;
justify-content: space-between;
gap: 20px;
@media (min-width: 576px) { 
    display: flex;
    flex-direction: row;
    align-items: flex-end;
 }
line-height: 40px;

.content__text {
    display: flex;
    align-items: center;
}
`;


const Button = styled.button`
vertical-align: text-bottom;
a {
    display: flex; 
    gap: 10px;
    align-items: center;
}
`;

