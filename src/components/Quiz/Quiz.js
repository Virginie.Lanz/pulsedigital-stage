import React, { useState } from 'react'
import questions from '../DataQuestions/DataQuestions'
import Plasmacom from '../Results/Plasmacom'
import NKCO from '../Results/NKCO'
import Rekovelle from '../Results/Rekovelle'
import Pulse from '../Results/Pulse'
import styled from 'styled-components'
import Header from '../Header/Header'

export default function Quiz() {

	const [currentQuestion, setCurrentQuestion] = useState(0);
	const [showScore, setShowScore] = useState(false);
	const [scorePlasmacom, setScorePlasmacom] = useState(1);
	const [scoreNKCO, setScoreNKCO] = useState(1);
	const [scoreRekovelle, setScoreRekovelle] = useState(1);
	const [scorePulse, setScorePulse] = useState(1);

	const handleAnswerOptionClick = (isSelected) => {
		if (isSelected === 'Plasmacom') {
			setScorePlasmacom(scorePlasmacom + 1);
			console.log('Plasmacom = ' + scorePlasmacom);
		}
		else if (isSelected === 'NKCO') {
			setScoreNKCO(scoreNKCO + 1);
			console.log('NKCO = ' + scoreNKCO);
		}

		else if (isSelected === 'Rekovelle') {
			setScoreRekovelle(scoreRekovelle + 1);
			console.log('Rekovelle = ' + scoreRekovelle);
		}

		else if (isSelected === 'Pulse') {
			setScorePulse(scorePulse + 1);
			console.log('Pulse = ' + scorePulse);
		}

		const nextQuestion = currentQuestion + 1;
		if (nextQuestion < questions.length) {
			setCurrentQuestion(nextQuestion);
		} else {
			setShowScore(true);
		}

	};

	function FinalResult() {
		if (
			scorePlasmacom >= scoreNKCO || 
			scorePlasmacom >= scoreRekovelle ||
			scorePlasmacom >= scorePulse

		) return (
			<Plasmacom/>
		)

		else if (
			scoreNKCO > scorePlasmacom || 
			scoreNKCO >= scoreRekovelle ||
			scoreNKCO >= scorePulse

		) return (
			<NKCO/>
		)

		else if (
			scoreRekovelle > scoreNKCO || 
			scoreRekovelle > scorePlasmacom ||
			scoreRekovelle >= scorePulse

		) return (
			<Rekovelle/>
		)

		else if (
			scorePulse > scoreNKCO || 
			scorePulse > scorePlasmacom ||
			scorePulse > scoreRekovelle

		) return (
			<Pulse/>
		)

	}

	return (
		<Global className="quiz">
			<Header/>
			{showScore ? (
				<Result className='quiz__result'>
					<FinalResult/>
				</Result>
			) : (
				<Questions>
					<div className='quiz__questions'>
						<div className='quiz--count'>
							<span>Question {currentQuestion + 1}</span>/{questions.length}
						</div>
						<div className='quiz--question'>{questions[currentQuestion].questionText}</div>
					</div>
					<div className='quiz--options'>
						{questions[currentQuestion].answerOptions.map((answerOption) => (
							<button onClick={() => handleAnswerOptionClick(answerOption.isSelected)}>{answerOption.answerText}</button>
						))}
					</div>
				</Questions>
			)}
		</Global>
	);
}

const Global = styled.div``;

const Questions = styled.div`
height: 100vh;
`;

const Result = styled.div``;
