import React from 'react';
import Homepage from './components/Pages/Homepage'
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Quiz from './components/Quiz/Quiz';
export default function App() {

	return (
		<div>
			<BrowserRouter>
				<Switch>
					<Route exact path="/">
						<Homepage/>
					</Route>
					<Route exact path="/quiz">
						<Quiz/>
					</Route>
				</Switch>
			</BrowserRouter>
		</div>

	);
}
